package id.co.firzil.instrat.helpers;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

import id.co.firzil.instrat.models.City;
import id.co.firzil.instrat.models.Interviewer;
import id.co.firzil.instrat.models.Params;

/**
 * Created by didik on 30/01/16.
 * Helper database
 */
public class Db {
    public static List<City> getAllCity() {
        return new Select().from(City.class).execute();
    }

    public static void generateCity() {
        ActiveAndroid.beginTransaction();
        try {
            new City("BANDUNG", "17").save();
            new City("bekasi", "14").save();
            new City("BOGOR", "13").save();
            new City("CIKAMPEK", "15").save();
            new City("CIREBON", "18").save();
            new City("DEPOK", "16").save();
            new City("DKI JAKARTA", "11").save();
            new City("MAKASSAR", "26").save();
            new City("MALANG", "23").save();
            new City("MEDAN", "24").save();
            new City("PALEMBANG", "25").save();
            new City("SEMARANG", "19").save();
            new City("SOLO", "21").save();
            new City("SURABAYA", "22").save();
            new City("TANGERANG", "12").save();
            new City("YOGYAKARTA", "20").save();
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<Interviewer> getAllInterviewer() {
        return new Select().from(Interviewer.class).execute();
    }

    public static void generateInterviewer() {
        ActiveAndroid.beginTransaction();
        try {
            for (int i = 1; i <= 50; i++) {
                String code;
                if (i >= 10) code = String.valueOf(i);
                else code = "0" + i;

                new Interviewer("BDG-0" + code).save();
                new Interviewer("BEK-0" + code).save();
                new Interviewer("BOG-0" + code).save();
                new Interviewer("CKP-0" + code).save();
                new Interviewer("CRB-0" + code).save();
                new Interviewer("DEP-0" + code).save();
                new Interviewer("JAK-0" + code).save();
                new Interviewer("MKS-0" + code).save();
                new Interviewer("MLG-0" + code).save();
                new Interviewer("MDN-0" + code).save();
                new Interviewer("PLG-0" + code).save();
                new Interviewer("SMG-0" + code).save();
                new Interviewer("SOL-0" + code).save();
                new Interviewer("SBY-0" + code).save();
                new Interviewer("TAN-0" + code).save();
                new Interviewer("YOG-0" + code).save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static Interviewer getInterviewer(String id) {
        return new Select()
                .from(Interviewer.class)
                .where("Code = ?", id)
                .executeSingle();
    }

    public static void clearParams(){
        new Delete().from(Params.class).execute();
    }

    public static List<Params> getAllParams(){
        return new Select().from(Params.class).execute();
    }
}
