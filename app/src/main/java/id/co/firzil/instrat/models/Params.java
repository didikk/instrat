package id.co.firzil.instrat.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by didik on 30/01/16.
 * Params
 */
@Table(name = "Params")
public class Params extends Model {
    @Column(name = "Key")
    private String key;

    @Column(name = "Value")
    private String value;

    public Params() {
        super();
    }

    public Params(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
