package id.co.firzil.instrat.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.instrat.R;
import id.co.firzil.instrat.activities.blok1.Soal1Activity;
import id.co.firzil.instrat.helpers.Db;
import id.co.firzil.instrat.helpers.FLog;

public class FieldworkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fieldwork);

        ButterKnife.bind(this);

        FLog.d("isiparams", Db.getAllParams().toString());
    }

    @OnClick(R.id.btn_audit_phone)
    void pilihPhone(){
        gotoSoal1();
    }

    @OnClick(R.id.btn_audit_modem)
    void pilihModem(){
        gotoSoal1();
    }

    @OnClick(R.id.btn_listing)
    void pilihListing(){
        gotoSoal1();
    }

    @OnClick(R.id.btn_advocay)
    void pilihAdvocacy(){
        gotoSoal1();
    }

    private void gotoSoal1(){
        startActivity(new Intent(this, Soal1Activity.class));
    }
}
