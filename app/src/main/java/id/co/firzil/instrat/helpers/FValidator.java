package id.co.firzil.instrat.helpers;

import android.widget.EditText;

/**
 * Created by didik on 30/01/16.
 * Validator
 */
public class FValidator {

    public boolean validate(EditText editText){
        if(editText.getText().toString().isEmpty()) {
            setError(editText, "Isian tidak boleh kosong");
            return false;
        }
        return true;
    }

    private void setError(EditText editText, String errorMessage){
        editText.setError(errorMessage);
        editText.requestFocus();
    }
}
