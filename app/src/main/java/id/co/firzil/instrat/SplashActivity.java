package id.co.firzil.instrat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import id.co.firzil.instrat.helpers.Db;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timerThread = new Thread() {
            public void run() {
                try {
                    // Beri waktu 1 detik
                    sleep(1500);
                    if (Db.getAllCity().size() <= 0) Db.generateCity();
                    if (Db.getAllInterviewer().size() <= 0) Db.generateInterviewer();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();
    }

}
