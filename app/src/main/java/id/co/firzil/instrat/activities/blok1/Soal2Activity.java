package id.co.firzil.instrat.activities.blok1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.instrat.R;
import id.co.firzil.instrat.activities.blok2.Soal3Activity;

public class zSoal2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal2);

        ButterKnife.bind(this);
    }

    public void onRadioButtonClicked(View view){
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        String pilihan;

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_1:
                if (checked) {
                   // Lakukan sesuatu
                    pilihan = getString(R.string.str2a);
                }
                break;
            case R.id.radio_2:
                if (checked) {
                    pilihan = getString(R.string.str2b);
                }
                break;
        }
    }

    @OnClick(R.id.btn_next)
    void next(){
        startActivity(new Intent(this, Soal3Activity.class));
    }
}
