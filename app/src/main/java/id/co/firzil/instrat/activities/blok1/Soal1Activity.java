package id.co.firzil.instrat.activities.blok1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.instrat.R;

public class Soal1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal1);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_exclusive)
    void pilihExclusive(){
        startActivity(new Intent(this, Soal2Activity.class));
    }

    @OnClick(R.id.btn_other)
    void pilihOther(){
        startActivity(new Intent(this, Soal2Activity.class));
    }

}
