package id.co.firzil.instrat.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by didik on 12/01/16.
 * Class helper untuk membantu urusan koneksi
 */
public class FNet {
    /**
     * Check network connection
     *
     * @param context
     *          Context that you want to check the connection
     * @return true if device connected to the network
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }
}
