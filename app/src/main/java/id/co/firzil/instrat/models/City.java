package id.co.firzil.instrat.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by didik on 30/01/16.
 * Model City
 */
@Table(name = "City")
public class City extends Model{
    @Column(name = "Name")
    private String name;

    @Column(name = "Code")
    private String code;

    public City() {
        super();
    }

    public City(String name, String code) {
        super();
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
