package id.co.firzil.instrat.activities.blok2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.instrat.R;

public class Soal4Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal4);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_next)
    void next(){
        startActivity(new Intent(this, Soal5Activity.class));
    }
}
