package id.co.firzil.instrat.helpers;


import android.util.Log;

import id.co.firzil.instrat.BuildConfig;

/**
 * Created by didik on 12/01/16.
 * Class helper untuk logging
 */
public class FLog {
    public static void d(String tag, String message){
        if(BuildConfig.DEBUG) Log.d(tag, message);
    }
}