package id.co.firzil.instrat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.co.firzil.instrat.activities.FieldworkActivity;
import id.co.firzil.instrat.helpers.Db;
import id.co.firzil.instrat.models.City;
import id.co.firzil.instrat.models.Params;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.spin_kota)
    Spinner spinKota;

    @Bind(R.id.et_nama)
    EditText etNama;

    private String kota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        spinKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                kota = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });

        List<String> cities = new ArrayList<>();

        for (City city : Db.getAllCity()) cities.add(city.getName());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, cities);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinKota.setAdapter(adapter);

    }

    @OnClick(R.id.btn_new)
    void submit() {
        String idInt = etNama.getText().toString();

        if (idInt.isEmpty()){
            etNama.requestFocus();
            etNama.setError("Id Interviewer tidak boleh kosong");
        }
        else if (Db.getInterviewer(idInt.toUpperCase()) != null) {
            new Params("A", idInt).save();
            new Params("B", kota).save();
            startActivity(new Intent(this, FieldworkActivity.class));
        }else Toast.makeText(MainActivity.this, "Id interviewer tidak cocok", Toast.LENGTH_SHORT).show();
    }
}
