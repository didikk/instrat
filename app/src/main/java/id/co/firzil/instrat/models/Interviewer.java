package id.co.firzil.instrat.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by didik on 30/01/16.
 * Interviewer
 */
@Table(name = "Interviewer")
public class Interviewer extends Model{
    @Column(name = "Code")
    private String code;

    public Interviewer() {
        super();
    }

    public Interviewer(String code) {
        super();
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
